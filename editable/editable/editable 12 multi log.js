// 1) Part 1: btnURL fill and click 
//      => Create TableData HTML 
//      => and init following variables
//
const btnURL = document.getElementById('btnURL');
btnURL.addEventListener('click', createTableURL );

let pathURL = "";
let tableData_init = tableData = tableData_filtreIn = tableData_filtreOut = [];
const tableDataHTML = document.getElementById('areaTable');
// const ligneHeaderHTML = document.getElementById('areaLigneHeader');
// const ligneValuesHTML = document.getElementById('areaLigneValues');

// CSS color code
const color_CyanDark = "darkcyan";
const color_Green = "rgb(77, 197, 77)";
const color_GreenLigth = "rgb(127, 227, 127)";
const color_Yellow = "yellow";
const color_RedLigth = "rgb(255, 200, 200)";

// 2) Part 2: Several possible option on aviable elt listener 
//      => Event on InputVal all values of TableData 
//      => Event on Table Line 2 (and 3 if needed)
//
let listeInputValListener = {};
let listeSelectActionListener = {};
let listeInputActionListener = {};

// === init Array TableData and print HTML Table ===============
//
function createTableURL(event) { 
    pathURL = document.getElementById('urlDataJSON').value;
    console.log("pathURL = ", pathURL);
    var title_url = document.getElementById('selectedURL');
    title_url.innerText = pathURL;

    createTableDataHTML(pathURL);
}

// Load tableData from data.JSON at pathURL ==============
//
async function createTableDataHTML(pathURL) {
    if( pathURL !== "") {
        const response = await fetch(pathURL);
        tableData_init = await response.json();
        tableData = tableData_init;
        tableData_filtreIn= null;
        tableData_filtreOut= null;

        printHTMLTableData();

        console.log("\n tab1 =",tableData); 

        // Event Listener sur toutes les values de tableData 
        // (affichées dans les cases 'inputVal')
        listeInputValListener = document.getElementsByClassName('inputVal');
        for (const inputVal of listeInputValListener) {
            inputVal.addEventListener('change', updateVal);
        }

        // Event Listener sur toutes actions (filter / sort / ...) 
        listeSelectActionListener = document.getElementsByClassName('selectAction');
        for (const selectAction of listeSelectActionListener) {
            selectAction.addEventListener('change', actionChoice);
        }

        listeInputActionListener = document.getElementsByClassName('inputAction');
        for (const inputAction of listeInputActionListener) {
            inputAction.addEventListener('change', actionChoiceParam);
        }
    }
}

// Create / print tableData HTML =============================
//
function printHTMLTableData() {
    console.log("\n createTableDataHTML");

    // Definir la grid TableData
    // Change Style with JS: method 1
    //
    const style_grid={
        display: "grid",
        gridTemplateColumns: `repeat(auto-fill, ${Object.keys(tableData[0]).length} , minmax(30px, 120px))`,
        gridTemplateRows: `repeat(auto-fill, ${tableData.length}, minmax(30px, 120px))`
        // gridTemplateRows: `repeat(${tableData.length}, minmax(30px, 120px))`
    }
    Object.assign(tableDataHTML.style,style_grid);

    // print line 1: header key name
    printHTMLTableHeader(tableData[0]);
    // print line 2 and 3: possible action
    printHTMLTableAction(tableData[0]);
    // print line 4->n: values tableData[0] to [n]
    printHTMLTableValues(tableData);
}

// tableData HTML: Line1 Header array key names -------------------
//
function printHTMLTableHeader(data0) {
    console.log(" printHTMLTableHeader ");

    let col=0;
    for (let key in data0) {
        var eltKey = document.createElement('h4');
        eltKey.setAttribute('id', `keyVal_1_${col}`);
        eltKey.innerText = key;
        eltKey.style.gridColumn = `${col+1}`;
        eltKey.style.gridRow = `1`;
        eltKey.style.backgroundColor = color_CyanDark;
        eltKey.style.textAlign = "center";
        eltKey.style.padding = "5px 0px 5px 0px";
        eltKey.style.border = "1px solid black";

        tableDataHTML.appendChild(eltKey);
        col++;
    }
}

// tableData HTML:  Line2 selectAction    ----------------------
//                  Line3 actionParam if required
//
function printHTMLTableAction(data0) {
    // console.log(" printHTMLTableValues ");
    // Print Line2: selectAction 
    let col=0;
    for (let key in data0) {
        // Print Line 2: Choice / Action select 
        var eltChoice = document.createElement('select');
        eltChoice.classList.add("selectAction");
        eltChoice.setAttribute('id', `ValChoix_2_`+key);
        eltChoice.setAttribute('col_name', key);
        eltChoice.style.gridColumn = `${col+1}`;
        eltChoice.style.gridRow = `2`;
        eltChoice.style.backgroundColor = color_Green;

        eltChoice[0] = new Option("Action...", "Action...", true, true);
        eltChoice[1] = new Option("Trier A-Z", "Trier A-Z", false, false);
        eltChoice[2] = new Option("Trier Z-A", "Trier Z-A", false, false);
        eltChoice[3] = new Option("Trier annul", "Trier annul", false, false);
        eltChoice[4] = new Option("Filtrer", "Filtrer", false, false);
        eltChoice[5] = new Option("Col. taille", "Col. taille", false, false);
        eltChoice[6] = new Option("Col. insert", "Col. ajout", false, false);

        tableDataHTML.appendChild(eltChoice);

        // Print Line 3: Choice / Action detail: inputAction if required
        // 1) set filter
        // 2) set column range
        // ...
        var eltActionParam = document.createElement('input');
        eltActionParam.classList.add("inputAction");
        eltActionParam.setAttribute('id', 'ValParam_3_'+key);
        eltActionParam.setAttribute('col_name', key);
        eltActionParam.setAttribute('col_num', col);
        eltActionParam.setAttribute('actionVal', "Action...");
        eltActionParam.style.gridColumn = `${col+1}`;
        eltActionParam.style.gridRow = `3`;
        eltActionParam.style.display = "none";
        eltActionParam.style.backgroundColor = color_GreenLigth; // annulable

        tableDataHTML.appendChild(eltActionParam);
        col++;
    }
}

// tableData HTML:  Line4 to n+4 values of table --------------
//
function printHTMLTableValues(tableData) {
    console.log(" printHTMLTableValues ");

    let num_ligne=0;
    for( const ligne_i of tableData) {
        printHTMLLigneValues(ligne_i,num_ligne); 
        num_ligne++;
    }
}

// tableData HTML:  Line_i+4 of values form elt_i of table --------
//
function printHTMLLigneValues(data,num_ligne) {
    // console.log(" printHTMLTableVal ", data);

    let col=0;
    for( const key in data) {
        var eltVal = document.createElement('textarea');
        eltVal.classList.add("inputVal");
        eltVal.setAttribute('id', `Val_${num_ligne}_`+key);
        eltVal.setAttribute('num_ligne', `${num_ligne}`);
        eltVal.setAttribute('num_col', `${col}`);
        eltVal.setAttribute('col_name', key);
        eltVal.type = "text";
        eltVal.value = `${data[key]}`;
        eltVal.style.gridColumn = `${col+1}`;
        eltVal.style.gridRow = `${num_ligne+4}`;
        
        tableDataHTML.appendChild(eltVal);
        col++;
    }
}

// Event listen function ===================================
// Event on inputVal: update value --------------------
//
function updateVal(event) {
    console.log("\n updateVal")
    var newVal = event.target.value;
    const num_ligne = event.target.getAttribute("num_ligne");
    const col_name = event.target.getAttribute("col_name");
    // const ex_val = tableData[num_ligne][col_name];
    tableData[num_ligne][col_name] = newVal;
    event.target.style.backgroundColor = color_RedLigth;
    //valuesEdited[] = num_ligne col_name
}

// Event on selectAction: -------------------------------------
//
function actionChoice(event) {
    console.log("\n actionChoice")
    var selectionVal = event.target.value;
    const col_name = event.target.getAttribute("col_name");
    // console.log("\n selectionVal = ", selectionVal);
    // console.log("\n col_name = ", col_name);

    switch (selectionVal) {
        case 'Trier A-Z':
            console.log("\n 1) Trier A-Z");
            sortAvoidOther(col_name);
            sortTableDataAZ(col_name);
            break;
        case 'Trier Z-A':
            console.log("\n 2) Trier Z-A");
            sortAvoidOther(col_name);
            sortTableDataZA(col_name);
            break;
        case 'Filtrer':
            console.log("\n 3) Filtrer");
            eltVal = document.getElementById('ValParam_3_'+col_name);
            eltVal.style.display = "flex";
            eltVal.setAttribute('actionVal', "Filtrer");
            eltVal.style.backgroundColor = color_GreenLigth; 
            eltVal.value="Search...";
            break;

        case 'Col. taille':
            console.log("\n 4) range Col.");
            // function
            // eltDiv = document.getElementById('DivParam_3_'+col_name);
            // eltDiv.style.display = "flex";
            // eltDiv.style.backgroundColor = color_Yellow;
            eltVal = document.getElementById('ValParam_3_'+col_name);
            eltVal.style.display = "flex";
            eltVal.setAttribute('actionVal', "Col. taille");
            eltVal.style.backgroundColor = color_GreenLigth; 
            eltVal.value="100";
            break;

        default:
            console.log(`No choice defined`);
            // function
            eltDiv = document.getElementById('DivParam_3_'+col_name);
            eltDiv.style.display = "none";
            // eltDiv.style.backgroundColor = color_Yellow;
            eltVal = document.getElementById('ValParam_3_'+col_name);
            eltVal.setAttribute('actionVal', "Action...");
            // eltVal.style.backgroundColor = color_GreenLigth; 
            eltVal.value="Action...";    
        }
}

// Event on inputAction additional Param -------------------------
//  when required to perform selectAction(Filtrer, Col. taille) 
//
function actionChoiceParam(event) {
    console.log("\n actionChoiceParam");
    var paramVal = event.target.value;
    const col_name = event.target.getAttribute("col_name");
    const col_num = event.target.getAttribute("col_num");
    const selectedAction = event.target.getAttribute("actionVal");

    switch (selectedAction) {
        case 'Filtrer':
            console.log("\n 3.2) Filtrer");
            filterNbTableData();
            break;
        case 'Col. taille':
            console.log("\n 4.2) range Col.");
            rangeColTableData(paramVal, col_name, col_num);
            break;
        default:
            console.log(`No choice defined`);
    }
}

// === Function on TableData HTML display none / flex ====================
//
function displayOnLine(num_ligne) {
    console.log('\n displayOnLine ', Object.keys(tableData[0]).length);
    // for(const key in tableData[0]) {
    //     // console.log('\n col ', col);
    //     // const eltVal = document.getElementById('Val_'+num_ligne+'_'+key);
    //     // eltVal.style.display = "flex";
    //     document.getElementById('Val_'+num_ligne+'_'+key).style.display = "flex";
    //     // console.log('\n eltDiv ', eltDiv);
    // }
    key0 = Object.keys(tableData[0])[0];
    console.log("key0 = ", key0);
    if( document.getElementById('Val_'+num_ligne+'_'+key0).style.display === "none") {
        for(let key in tableData[0]) {
            // console.log('\n col ', col);
            // const eltDiv = document.getElementById('Val_'+num_ligne+'_'+key);
            // eltDiv.style.display = "none";
            document.getElementById('Val_'+num_ligne+'_'+key).style.display = "flex";
            // const eltVal = document.getElementById('Val_'+num_ligne+'_'+key);
            // eltVal.style.display = "none";
            // console.log('\n eltDiv ', eltDiv);
        }
    }
}
function displayOnAllLine() {
    console.log('\n displayOnAllLine ');
    for(let num_ligne = 0; num_ligne<tableData.length; num_ligne++) {
        displayOnLine(num_ligne);
    }
}
function displayOffLine(num_ligne) {
    console.log('\n displayOffLine ', Object.keys(tableData[0]).length);
    key0 = Object.keys(tableData[0])[0];
    console.log("key0 = ", key0);
    if( document.getElementById('Val_'+num_ligne+'_'+key0).style.display !== "none") {
        for(let key in tableData[0]) {
            // console.log('\n col ', col);
            // const eltDiv = document.getElementById('Val_'+num_ligne+'_'+key);
            // eltDiv.style.display = "none";
            document.getElementById('Val_'+num_ligne+'_'+key).style.display = "none";
            // const eltVal = document.getElementById('Val_'+num_ligne+'_'+key);
            // eltVal.style.display = "none";
            // console.log('\n eltDiv ', eltDiv);
        }
    }
}


// === OPTION: Filter / Search  ============================
//

// For the moment Search in one colonne
// When reset or search "" display ON all the line even if multiple col search
let ColOn=[];
let getColNum = {};

function initAllFilterCol(ColOn) {
    for(let col=0; col<= Object.keys(tableData[0]).length; col++) {
        ColOn[col] = [-1]    // -1 : all is display On
    }
}

// function filterTableData(paramVal, col_name) {
//     console.log('\n filterTableData ', paramVal);
//     initAllFilterCol(ColOn);
//     if( paramVal ==="Search..." ) {
//         ColOn[col_num] = [-1];      // Cancel search on this col_name
//         displayOnAllLine();
//         return -1;                  // -1 : all is display On
//     } else if( paramVal ==="" || paramVal ===" " ) {
//         ColOn[col_num] = [-1];      // Cancel search on this col_name
//         displayOnAllLine();
//         return -1;                  // -1 : all is display On

//     } else {
//         ColOn[col_num] = [0];      // New search: 0 = 0 found init
//         for(let num_ligne = 0; num_ligne<tableData.length; num_ligne++) {
//             var colVal = tableData[num_ligne][col_name];
//             if( colVal.includes(paramVal) ) {
//                 ColOn[col_num][0] += 1;
//                 ColOn[col_num][num_ligne+1] = 1;
//                 tableData_new.push(tableData[num_ligne]);
//             } else {
//                 displayOffLine(num_ligne);
//             }
//         }
//     }
// }

function filterNbTableData() {
    console.log('\n filterNbTableData ');

    // 1) Find or update all paramFilter to check
    let allParam = []; //key_i, paramVal_i, ... key_n, paramVal_n, ... 
    for(const key in tableData[0]) {
        const paramVal = document.getElementById('ValParam_3_'+key).value;
        if( paramVal !=="Search..." && paramVal !=="" && paramVal !==" " && paramVal !=="  ") {
            allParam.push( key );
            allParam.push( paramVal );
        }
    }

    // 2) Compute tableData_filtred
    tableData_filtreIn = [];
    tableData_filtreOut = [];
    console.log("allParam = ", allParam);
    if(allParam.length === 0) {
        tableData_filtreIn = tableData;
        tableData_filtreOut = null;
        displayOnAllLine();
    } else {
        // check and:   push in tableData_new
        //      or:     displayOff 
        for(let num_ligne = 0; num_ligne<tableData.length; num_ligne++) {

            let ligneOn = true;
            for(let k=0; k<allParam.length; k++) {
                const key = allParam[k];
                k++;
                const paramVal = allParam[k];
                var colVal = tableData[num_ligne][key].toString();
                console.log("paramVal colVal = ", paramVal, " - ", colVal );
                if( colVal.includes(paramVal) === false ) {
                    ligneOn = false;
                    break;
                }
            }
            if( ligneOn ) {
                displayOnLine(num_ligne);
                tableData_filtreIn.push( tableData[num_ligne] );
            } else {
                tableData_filtreOut.push( tableData[num_ligne] );
                displayOffLine(num_ligne);
            }
        }
    }

    // 3) if Param ++ : no need to sort

    // 4) if Param -- : new line added, maybe unsorted... sort needed!
    for(const key in tableData[0]) {
        var valChoice = document.getElementById(`ValChoix_2_`+key).value;
        if(valChoice.includes("Trier")) {
            sortTableDataAZ(key);
            break;
        }
    }
    // PLUS FACIL SI
    // 1) Calcul du nouveau tableau
    // 2) Effacer tous les inputVal
    // 3) print newTable
}

// === OPTION: Sort ======================================================
//
function sortAvoidOther(col_name) {
    // Print "Off" === "Action..." for the other sort columns
    for(const key in tableData[0]) {
        if(key !== col_name) {
            var eltChoice = document.getElementById(`ValChoix_2_`+key);
            var valChoice = eltChoice.value;
            if(valChoice.includes("Trier")) {
                eltChoice.value = "Action...";
                break;
            }
        }
    }
}

function sortTableDataAZ(col_name) {
    // if tableData_filtred.length === 0 return false; !!!!!!!
    // Create selectCol to sort
    // console.log("sortTableDataAZ");
    let tableData_toSort = tableData;
    let tableData_off = [];
    if( tableData_filtreIn !== null) {
        tableData_toSort = tableData_filtreIn;
        tableData_off = tableData_filtreOut;
    } 
    // else {
        // tableData_toSort = tableData_filtred
    // }
    let selectCol = [];
    let num_ligne = 0;
    for (const data of tableData_toSort) {
        let ligneNum_colVal = {
            ligneNum: num_ligne,
            colVal: data[col_name]
        }
        selectCol.push(ligneNum_colVal);
        num_ligne++;
    }
    // console.log("col length = ",selectCol.length);

    // Sort the tab
    selectCol.sort((a,b) => (a.colVal > b.colVal) ? 1 : ((b.colVal > a.colVal) ? -1 : 0))
    // console.log("col sorted = ",selectCol);

    // deleteTableDataHTML(); !!! 
    removeElementsByClass('inputVal');

    // Display HTML new Table
    // tableData_sorted = [];
    num_ligne = 0;
    for(const col of selectCol) {
        // console.log("col i", col.ligneNum);
        let data = tableData_toSort[ col.ligneNum ];
        // tableData_sorted.push(data);
        printHTMLLigneValues(data, num_ligne);
        num_ligne++;
    }
    for(const data of tableData_filtreOut) {
        printHTMLLigneValues(data, num_ligne);
        displayOffLine(num_ligne);
        num_ligne++;
    }
}

function removeElementsByClass(className){
    var elements = document.getElementsByClassName(className);
    while(elements.length > 0){
        elements[0].parentNode.removeChild(elements[0]);
    }
}